---
layout: post
title: Контакты
description: Никаких услуг пока не оказываю. Могу только немного помочь разобраться в тех или иных вопросах (если компетентен в них конечно). Есть вопросы? Предложения? Хотите просто поговорить? Нет проблем, пишите!)
image: /assets/final_logo.PNG
modified: 2024-10-15
---

# Ссылки, связь со мной

Никаких услуг пока не оказываю. Могу только немного помочь разобраться в тех или иных вопросах (если компетентен в них конечно). Есть вопросы? Предложения? Хотите просто поговорить? Нет проблем, пишите!)

## Мессенджеры

<i class="icon-matrix-org"></i> [@seva77:matrix.org](https://matrix.to/#/@seva77:matrix.org){: rel="nofollow noopener" target="_blank"}
{: title="Matrix"}

<i class="icon-xmpp"></i> seva77@404.city
{: title="XMPP"}

## Соц. сети

<i class="icon-gotosoc"></i> [gts.station77.space/@seva77](https://gts.station77.space/@seva77){: rel="nofollow noopener" target="_blank"}
{: title="GoToSocial (доступен из любой федеративной соц. сети на ActivityPub)"}

<i class="icon-friendica"></i> [venera.social/u/seva77](https://venera.social/u/seva77) (заброшен, запасной на случай поломки GTS)
{: title="Friendica (доступен из любой федеративной соц. сети на ActivityPub или Diaspora)"}

<i class="icon-twitter"></i> [x.com/4SEVA77](https://x.com/4SEVA77){: rel="nofollow noopener" target="_blank"}
{: title="Twitter"}

<i class="icon-github"></i> [github.com/SEVA77](https://github.com/SEVA77){: rel="nofollow noopener" target="_blank"}
{: title="Github"}

## Почта

<i class="icon-mail"></i> <a target="_blank" href="mailto:CEBA1996@outlook.com?subject=По ссылке из My77thBlog">CEBA1996@outlook.com</a>

***

Также ниже оставлю форму комментариев для ваших вопросов, пожеланий, замечаний и пр. На этой странице модерируются (ожидают одобрения) все новые комментарии и если хотите пообщаться без таких ограничений, для этого есть [специальная страница](/projects/demo/test_comments/){: rel="nofollow noopener" target="_blank"}. В комментариях старайтесь быть адекватными и уважать друг друга. Спасибо!
