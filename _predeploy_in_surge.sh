#!/bin/bash

# $ sh _predeploy_in_surge.sh

export JEKYLL_ENV=production
export LC_ALL=C.UTF-8

echo "Build site:"
bundle exec jekyll build

echo "The current directory is:"
pwd
cd _site/
echo "The next current directory is:"
pwd
echo "Files in the directory:"
ls
echo

rm yandex_2ecd0416365c5472.html
rm googlecc4fe017defa7332.html

rm robots.txt
echo -e "User-agent: *\nDisallow: /" >robots.txt
echo "robots.txt is modified"

#rss.xml and sitemap.xml

sed -i "s/my77thblog.pp.ua/my77thblog.surge.sh/" "rss.xml"
echo "rss.xml is modified"
sed -i "s/my77thblog.pp.ua/my77thblog.surge.sh/" "sitemap.xml"
echo "sitemap.xml is modified"

echo my77thblog.surge.sh > CNAME
echo "CNAME is created"

echo
echo "Deploy in surge.sh:"
echo

surge
