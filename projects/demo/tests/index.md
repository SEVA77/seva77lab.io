---
layout: post
title: Тесты всего
description: Тесты всего
image: /assets/final_logo.PNG
comments: 'false'
video_url: https://invidious.osi.kr
---

# Тесты всего

## Тест влияния заголовков iframe

Выбираем сайт, позволяющий встраивать свое видео в iframe (сейчас встроен <b>{{ page.video_url }}/</b>):

<div class="video-container">
<iframe id='ivplayer' src='{{ page.video_url }}/embed/aZTIO6F7R_4?autoplay=0&player_style=youtube' style='border:none;' allowfullscreen></iframe>
</div>
