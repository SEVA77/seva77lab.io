---
layout: post
title: Базовые настройки браузерных расширений
description: Инструкции для начальной настройки некоторых браузерных расширений в дополнение к моим прошлым и, скорее всего, будущим статьям.
categories: instructions
catname: Мелкие инструкции
caturl: "/blog/instructions/index.html"
image: "/images/instructions/2023/2023-12-10-bazovye-nastrojki-brauzernyh-rasshirenij/1.jpg"
thumb: "/images/instructions/2023/2023-12-10-bazovye-nastrojki-brauzernyh-rasshirenij/1b.jpg"
permalink: "blog/:categories/:year-:month-:day-:slug.html"
---

# {{ page.title }}
{:.no_toc}

![Пазл]({{ page.image }}){: style="filter: grayscale(.4)"}

["Scattered puzzle pieces next to solved fragment"](https://www.flickr.com/photos/10361931@N06/4273913228){: rel="nofollow noopener" target="_blank"}
 by 
[Horia Varlan](https://www.flickr.com/photos/10361931@N06){: rel="nofollow noopener" target="_blank"}
 is licensed under 
[CC BY 2.0](https://creativecommons.org/licenses/by/2.0/){: rel="nofollow noopener" target="_blank"}
{: style="font-size: 0.7em;font-style: italic;"}

{{ page.description }}

Дабы впредь не встраивать в статьи инструкции, не относящиеся к теме, буду пробовать выносить их в отдельные, но в то же время полноценные статьи. Конкретно на данный момент инструкции идут в дополнение к статье о [базовых программах на винду](/blog/software-review/2021-12-31-programmy-neobhodimye-posle-ustanovki-windows.html){: rel="nofollow noopener" target="_blank"} и в дальнейшем могут также пойти в дополнение к аналогичной статье про линукс (если у меня дойдут руки хотя б попробовать ее написать). Однако в то же время мне ничто не мешает добавлять сюда всякие разные инструкции к другим расширениям, не относящемся к какой-либо статье.

<i class="icon-attention"></i> Некоторые инструкции в статье взяты из сторонних источников, которые будут перечислены в конце. <br><br>

## Настройка uBlock Origin (AdNauseam)

### Убрать кнопку "Premium" и "Подлинные организации" в X (бывший твиттер)

Добавить в фильтр:

```
x.com###react-root > div > div > div > header > div > div > div > div > div > nav > a:has-text("Premium")
x.com###react-root > div > div > div > header > div > div > div > div > div > nav > a:has-text("Подлинные организации")
```

В говне с меняющимися рандомносгенерированными классами приходится вот так в тупую проходиться по дереву тегов.

Туда же блок правой колонки "Подпишитесь на Premium":

```
x.com###react-root > div > div > div > main > div > div > div > div > div > div > div > div > div > div:has-text("Premium")
```

### Скрыть YouTube Shorts из YouTube

Список фильтров uBlock Origin, скрывающий все следы коротких видеороликов Shorts на YouTube: <https://github.com/gijsdev/ublock-hide-yt-shorts>{: rel="nofollow noopener" target="_blank"}

### Блокировка «третьих» ресурсов

Делаем из uBO подобие uMatrix.

- в настройках отмечаем «I am an advanced user» и в настройках этого пункта ставим значение filterAuthorMode на «true»;
- повышаем уровень блокировки [на medium](https://github.com/gorhill/uBlock/wiki/Blocking-mode:-medium-mode#how-to-enable-this-mode){: rel="nofollow noopener" target="_blank"} и в разделе «Моих правил» добавляем эти правила:

```
* * 3p-frame block
* * 3p-script block
* a.akamaiedge.net * noop
* ajax.googleapis.com * noop
* akamai.net * noop
* ajax.aspnetcdn.com * noop
* b-cdn.net * noop
* libs.baidu.com * noop
* apps.bdimg.com * noop
* cdn.bootcss.com * noop
* cloudflare.com * noop
* cloudflare.net * noop
* cloudflareinsights.com * noop
* cloudfront.net * noop
* cdn.cookielaw.org * noop
* disqus.com * noop
* fastly.net * noop
* sdn.geekzu.org * noop
* googlevideo.com * noop
* gstatic.com * noop
* code.jquery.com * noop
* cdn.jsdelivr.net * noop
* lib.sinaapp.com * noop
* ajax.microsoft.com * noop
* pussthecat.org * noop
* raw.githubusercontent.com * noop
* sndcdn.com * noop
* themoviedb.org * noop
* tmdb.org * noop
* upcdn.b0.upaiyun.com * noop
* ajax.proxy.ustclug.org * noop
* yandex.net * noop
* yandex.st * noop
* yastatic.net * noop
```
{:.no_hlt}

(сначала в правую колонку → сохраняем → утверждаем).

В безопасности удобно не бывает, поэтому будьте готовы к периодическим поломкам сайтов и добавлению их в «белые списки» uBlock — отмечаем серым в 3-й колонке (см. [локальные динамические фильтры](https://github.com/gorhill/uBlock/wiki/Dynamic-filtering:-quick-guide){: rel="nofollow noopener" target="_blank"}).

## Настройка LocalCDN

Настройка работы совместно с блокировщиком uBlock Origin.

- по пути «Настройки» → «Расширенные» включаем «Блокировать запросы недостающих ресурсов»;
- там же отмечаем uBlock в «Сгенерировать правила для установленного блокировщика рекламы» и копируем правила в раздел «Мои правила» нашего блокировщика (сначала в правую колонку → сохраняем → утверждаем).

Протестить можно на их сайте: <https://www.localcdn.org/test>{: rel="nofollow noopener" target="_blank"}

***

*\* [Источник инструкций блокировки "третьих" ресурсов для uBO и LocalCDN](https://t.me/c/1215587418/396){: rel="nofollow noopener" target="_blank"}.*


&nbsp;{{ page.date | date: '%d' }}.{{ page.date | date: '%m' }}.{{ page.date | date: '%Y' }}&nbsp;<i class="icon-calendar"></i>
{: style="font-size:0.8em;font-style:italic;direction:rtl"}
