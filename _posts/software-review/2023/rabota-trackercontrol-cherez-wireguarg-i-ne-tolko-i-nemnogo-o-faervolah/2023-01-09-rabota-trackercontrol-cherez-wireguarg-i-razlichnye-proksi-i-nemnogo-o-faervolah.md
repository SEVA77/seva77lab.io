---
layout: refresh-to
title: Работа TrackerControl через Wireguard и различные прокси и немного о фаерволах
description: Замечательная программа контроля трафика приложений TrackerControl на андроиде с возможностью резать всевозможные трекеры и рекламу, которую мне необходимо было заставить работать через Wireguard и я нашёл, как. Делюсь инструкцией на своём примере.
categories: software-review
image: "/images/software-review/2023/2023-01-09-rabota-trackercontrol-cherez-wireguard-i-razlichnye-proksi-i-nemnogo-o-faervolah/1.jpg"
permalink: "blog/:categories/:year-:month-:day-:slug.html"
correct_url: "/blog/software-review/2023-01-09-rabota-trackercontrol-cherez-wireguard-i-razlichnye-proksi-i-nemnogo-o-faervolah.html"
pagination: 
  enabled: false
---
